/*
 * Mi segunda clase java
 * 
 * La clase triangulo permite almacenar un círculo
 * además de su posición en la pantalla, si está relleno o no,
 * su color etc
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 *
 * @author Administrator
 */
public class Triangulo extends Polygon{
    
    public Color color = null;
    public boolean relleno = false;
    int x = 0;
    int y = 0;
    
    public Triangulo (int _x, int _y, int _width, Color _color, boolean _relleno) {
        this.x = _x;
        this.y = _y;
        this.npoints = 3;
        this.xpoints[0] = _x;
        this.ypoints[0] = _y - _width/4;
        
        this.xpoints[1] = _x + _width/3;
        this.ypoints[1] = _y + _width/3;
        
        this.xpoints[2] = _x - _width/3;
        this.ypoints[2] = _y + _width/3;
        this.color = _color;
        this.relleno = _relleno;
    }
    public void pintaYColorea(Graphics2D g2){
        //leo el color del circulo
            g2.setColor(this.color);
            if (this.relleno) {
                g2.fill(this);
            } else {
                g2.draw(this);
            }
    }

    void redimensiona(int radio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
